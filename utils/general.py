from datetime import datetime, date
from decimal import Decimal

def json_build(data):
    if isinstance(data, dict):
        for k, v in data.iteritems():
            data[k] = json_build(v)

    if isinstance(data, list):
        for i, v in enumerate(data):
            data[i] = json_build(v)

    if isinstance(data, tuple):
        data = json_build([item for item in data])

    if isinstance(data, datetime):
        data = str(data)

    if isinstance(data, date):
        data = str(data)

    if isinstance(data, long):
        data = str(data)

    if isinstance(data, Decimal):
        data = str(data)

    return data

def to_dict(obj, class_key=None):
    if isinstance(obj, dict):
        for k in obj.keys():
            obj[k] = to_dict(obj[k], class_key)
        return obj
    elif hasattr(obj, "__iter__"):
        return [to_dict(v, class_key) for v in obj]
    elif hasattr(obj, "__dict__"):
        data = dict([(key, to_dict(value, class_key))
        for key, value in obj.__dict__.iteritems()
        if not callable(value) and not key.startswith('_')])
        if class_key is not None and hasattr(obj, "__class__"):
            data[class_key] = obj.__class__.__name__
        return data
    else:
        try:
            return str(obj)
        except UnicodeEncodeError:
            return str(obj.encode('utf-8'))
