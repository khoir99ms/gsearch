__author__ = 'Khoir MS'
import re
import chardet
from lxml.html import clean, fromstring, tostring

prerem_tag_list = [
    'acronym', 'address', 'applet', 'area', 'base', 'basefont', 'big', 'body', 'col', 'colgroup', 'del', 'dfn', 'dir', 'head', 'html', 'ins', 'isindex', 'kbd', 'link', 'map', 'menu', 'meta', 'object', 'optgroup', 'param', 's', 'samp', 'tt', 'var'
]

postrem_tag_list = [
    'a', 'span', 'abbr', 'b', 'cite', 'dl', 'em', 'fieldset', 'font', 'form', 'frame', 'frameset', 'i', 'iframe', 'img', 'input', 'hr', 'ol', 'select', 'small', 'strike', 'strong', 'sub', 'sup', 'table', 'tbody', 'tfoot', 'tr', 'u', 'ul', 'source'
]

rep_tag_list = [
    'blockquote', 'article', 'section', 'header', 'bdo', 'center', 'code', 'dd', 'div', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li', 'pre', 'td', 'wbr'
]

rem_ct_list = [
    'button', 'datalist', 'svg', 'canvas', 'nav', 'caption', 'dt', 'label', 'legend', 'figure', 'figcaption', 'noframes', 'noscript', 'option', 'script', 'style', 'textarea', 'th', 'thead', 'title', 'embed', 'aside', 'footer', 'hgroup', 'keygen', 'mark', 'meter', 'video', 'audio', 'time', 'canvas'
]
__iso_or_windows_encodings = re.compile(r'^(iso|windows)', flags=re.I)
rep_tag = re.compile(r'<(/)?(' + '|'.join(rep_tag_list) + ')\\b[^>]*>', flags=re.I)

def html_entities(__str__, decode=False):
    if not __str__: return None
    try:
        result = __str__
        if decode:
            charset = chardet.detect(result)
            if not charset['encoding']: charset['encoding'] = 'utf-8'
            if __iso_or_windows_encodings.search(charset['encoding']):
                result = result.decode('utf-8', 'ignore')
            else:
                result = result.decode(charset['encoding'], 'ignore')

        result = result.encode('ascii', errors='xmlcharrefreplace').strip()

        return result
    except Exception as e:
        print 'Error : {0} | {1}...'.format(str(e), __str__[0:20])
        raise

def strip_tags(__str__):
    text = re.sub(r'[\r\n\t]+', ' ', __str__)
    text = re.sub(r'[ ]+', ' ', text)
    text = re.sub(r'<!--[^>]+-->', '', text)
    text = re.sub(r'<!--\[if*<!\[endif\]-->', '', text)
    text = re.sub(r'<!doctype[^>]*>', '', text, flags=re.I)
    text = re.sub(r'<q[^>]*>', '"', text, flags=re.I)
    text = re.sub(r'</q>', '"', text, flags=re.I)

    text = rep_tag.sub(lambda pat: '</p>' if pat.group(1) is not None else '<p>', text)
    text = re.sub(r'<p[^>]*>', '<p>', text, flags=re.I).strip()
    text = re.sub(r'[ ]+', ' ', text).strip()
    text = re.sub(r'>[ ]+<', '><', text).strip()

    return text

def remove_overtags(__str__):
    text = re.sub(r'<p[^>]*?>([ ]*<br[^>]*?>[ ]*){0,}</p>', '', __str__, flags=re.I).strip()
    text = re.sub(r'<p[^>]*?>[ ]*</p>', '', text, flags=re.I).strip()
    text = re.sub(r'</p>([ ]*<br[^>]*?>[ ]*)*<p[^>]*>', '</p><p>', text, flags=re.I).strip()
    text = re.sub(r'([ ]*<br[^>]*>[ ]*){2,}', ' <br> ', text, flags=re.I).strip()
    text = re.sub(r'<p[^>]*?>([ ]*<br[^>]*?>[ ]*){1,}', '<p>', text, flags=re.I).strip()
    text = re.sub(r'(<br[^>]*?>[ ]*){1,}</p>', '</p>', text, flags=re.I).strip()
    text = re.sub(r'([ ]*<p>[ ]*){1,}', '<p>', text).strip()
    text = re.sub(r'([ ]*</p>[ ]*){1,}', '</p>', text).strip()
    text = re.sub(r'^([ ]*</p>[ ]*){1,}', '', text).strip()
    text = re.sub(r'^([ ]*<br[^>]*>[ ]*){1,}', '', text).strip()
    text = re.sub(r'^([ ]*<p>[ ]*){1,}', '<p>', text).strip()
    text = re.sub(r'([ ]*<p[^>]*>[ ]*){1,}$', '', text).strip()
    text = re.sub(r'([ ]*<br[^>]*>[ ]*){1,}$', '', text).strip()
    text = re.sub(r'([ ]*</p>[ ]*){1,}$', '</p>', text).strip()
    text = re.sub(r'\(([ ]?<[^<>]*?>){1,}', '(', text, flags=re.I).strip()
    text = re.sub(r'(<[^<>]*?>[ ]?){1,}\)', ')', text).strip()
    text = re.sub(r'<p>', ' <p>', text).strip()
    text = re.sub(r'[ ]+', ' ', text).strip()
    text = text.strip()

    return text

def cleaner(__str__, post=False):
    if post:
        # remove all tags except p & br tags after regex process
        cleaner = clean.Cleaner(remove_tags=postrem_tag_list)
    else:
        cleaner = clean.Cleaner(scripts = True, style = True, comments = True, page_structure = False, kill_tags=rem_ct_list, remove_tags=prerem_tag_list, safe_attrs_only = False)

    text = fromstring(__str__)
    text = cleaner.clean_html(text)
    text = tostring(text, encoding="ascii")
    text = rep_tag.sub(lambda pat: '</p>' if pat.group(1) is not None else '<p>', text)

    return text