__author__ = 'Khoir MS'

import re
import time
import urllib
import urlparse

from utils.general import json_build
from browser import Browser
from extractor import GSearch as EXTGsearch

class GSearch:

    def __init__(self, **kwargs):
        self.driver = Browser(**kwargs)
        self.gsearch = EXTGsearch()

        self.baselink = 'https://www.google.com'
        self.__locator_searchfield = 'input[name=q]'
        self.__locator_pager = '#foot td:not(.navend)'

        self.__is_page_loaded = False

    def search(self, query, page=0, sort_by=None):
        result = {'success': False}
        try:

            if not self.__is_page_loaded:
                self.driver.load_and_wait(url=self.baselink, selector=self.__locator_searchfield, wait=60)
                search = self.driver.get_element(selector=self.__locator_searchfield)
                if search:
                    search.clear()
                    search.send_keys(query)
                    self.driver.press_enter(search)
                    self.driver.wait_by_id(selector='resultStats', wait=60)

                self.sort_bydate(sort_by=sort_by)

            if page:
                retry = 0
                max_retry = 100
                while True:
                    if page > 10 and not self.__is_page_loaded:
                        pages = self.driver.get_elements(selector='{} > a'.format(self.__locator_pager))
                        if pages:
                            self.driver.click(pages[-1])
                            time.sleep(1)

                    page_n = self.driver.get_element(selector='{} > a[aria-label="Page {}"]'.format(self.__locator_pager, page))
                    if page_n:
                        self.driver.click(page_n)
                        time.sleep(1)
                        break
                    else:
                        curpage = self.driver.get_element(selector='#foot td:not(.navend).cur')
                        if curpage:
                            curpage = self.driver.get_value(curpage)
                            curpage = re.sub(r'[^0-9]', '', curpage, flags=re.U)
                            curpage = curpage.strip()
                            if str(curpage) == str(page):
                                self.driver.log('page {} is current page'.format(page))
                                break

                        if retry > max_retry:
                            break
                        retry += 1
                        self.driver.log('retry to {} for get page {}'.format(retry, page))

                    if not self.__is_page_loaded:
                        self.__is_page_loaded = True

            html = self.driver.get_element('html').get_attribute('outerHTML')
            self.gsearch.set_document(html)
            sresult = self.gsearch.search()

            result['data'] = sresult
            result['total'] = len(sresult)
            result['success'] = True
        except:
            self.driver.log(level='error')
            result['data'] = list()
            result['total'] = 0

        return result

    def sort_bydate(self, sort_by=None):
        accept_sort = ['h', 'd', 'w', 'm', 'y']
        if not (sort_by or sort_by in accept_sort): return

        url = self.driver.browser.current_url
        parseurl = urlparse.urlsplit(url)

        query = dict(urlparse.parse_qsl(urlparse.urlsplit(url).query))
        query['tbs'] = 'qdr:w,sbd:0'
        qstring = urllib.urlencode(query)
        url_sort = "{baselink}{path}?{query}".format(baselink=self.baselink, path=parseurl.path, query=qstring)
        self.driver.load_and_wait(url=url_sort, selector='#foot', wait=60)

if __name__ == '__main__':
    import json
    gs = GSearch()
    # gs.driver.set_proxies(proxy='103.75.25.102', port='3128')
    # gs = GSearch(driver='chrome', driver_path='/home/khoir/Workspace/chromedriver')
    page = 11
    # for page in range(1, 3):
    print json.dumps(gs.search('impor beras', page=page, sort_by='w'), indent=4)