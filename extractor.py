__author__ = 'Khoir MS'

import re
import urlparse

from pyquery import PyQuery as PyQ

from logger import Logger
from utils.html import html_entities, cleaner, strip_tags, remove_overtags

class Extractor(object):

    def __init__(self):
        self.logger = Logger(self.__class__.__name__)

        self.pyq = None
        self.document = None
        self.sections = None
        self.fields = None

    def set_document(self, doc, parser=None):
        self.document = doc
        self.pyq = PyQ(self.document, parser=parser)

    def extract_section(self, section, subsection):
        results = {}
        subsection_key = subsection.lower()
        container = self.sections[section][subsection]

        # self.logger.log('container selector :', container['selector'])
        if container['quantity'] == 'multiple':
            results[subsection_key] = []
            elements = self.pyq(container['selector'])
            for element in elements:
                row = {}
                for field in self.fields[section][subsection]:
                    record = self.fields[section][subsection][field]
                    row[field.lower()] = self.get_field_value(record, element)
                results[subsection_key].append(row)
        elif container['quantity'] == 'single':
            results[subsection_key] = self.get_field_value(container)
        return results

    def generator_extract_section(self, section, subsection):
        container = self.sections[section][subsection]

        # self.logger.log('container selector :', container['selector'])
        if container['quantity'] == 'multiple':
            elements = self.pyq(container['selector'])
            for element in elements:
                row = {}
                for field in self.fields[section][subsection]:
                    record = self.fields[section][subsection][field]
                    row[field.lower()] = self.get_field_value(record, element)
                yield row
        elif container['quantity'] == 'single':
            yield self.get_field_value(container)

    def get_field_value(self, record, parent=None):
        exit = None
        if not parent is None:
            if record['type'] == 'attr':
                if record['selector']:
                    exit = PyQ(parent)(record['selector']).attr(record['attr'])
                else:
                    exit = PyQ(parent).attr(record['attr'])
            elif record['type'] == 'text':
                if record['selector']:
                    exit = PyQ(parent)(record['selector']).text()
                else:
                    exit = PyQ(parent).text()
            elif record['type'] == 'html':
                if record['selector']:
                    exit = PyQ(parent)(record['selector']).html()
                else:
                    exit = PyQ(parent).html()
            elif record['type'] == 'style':
                exit = record['attr']
        else:
            if record['type'] == 'attr':
                exit = self.pyq(record['selector']).attr(record['attr'])
            elif record['type'] == 'text':
                exit = self.pyq(record['selector']).text()
            elif record['type'] == 'html':
                exit = self.pyq(record['selector']).html()
            elif record['type'] == 'style':
                exit = record['attr']
        if record['type'] in ['text', 'html']:
            exit = html_entities(exit)

        return exit

class GSearch(Extractor):

    def __init__(self):
        super(GSearch, self).__init__()

        self.__locator_item = 'div[role=main] div[data-hveid]:not([id^=imagebox])'
        self.__locator_url = 'h3 a'
        self.__locator_title = 'h3 a'
        self.__locator_desc = 'div[data-hveid] h3 + div > div > span'

    def search(self):
        result = list()
        items = self.pyq(self.__locator_item)
        for item in items:
            url = self.get_field_value({'selector': self.__locator_url, 'type': 'attr', 'attr': 'href'}, item)
            title = self.get_field_value({'selector': self.__locator_title, 'type': 'text'}, item)
            desc = self.get_field_value({'selector': self.__locator_desc, 'type': 'html'}, item)

            result.append({'url': url, 'title': title, 'description': desc})

        return result

if __name__=='__main__':
    import json
    gs = GSearch()
    html = open('test.html').read()

    gs.set_document(html)
    print json.dumps(gs.search(), indent=4)